Pod::Spec.new do |s|
  s.name             = 'ProfileTableViewController'
  s.version          = '0.1.0'
  s.summary          = 'Create ProfileTableViewController.'
  s.homepage         = 'https://gitlab.com/mahvish10/profiletableviewcontrollerpod'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mahvish10' => 'mahvishsyed15@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/mahvish10/profiletableviewcontrollerpod.git',:branch=> "main"}
  s.ios.deployment_target = '14.0'
  s.swift_version = '4.2'
  s.source_files = 'ProfileTableViewController/Classes/**/*.{swift,.h,.m}'
  s.resource_bundles = {
          'ProfileTableViewController' => ['ProfileTableViewController/Classes/**/*.{xib,storyboard,xcassets}']
      }
  s.dependency 'SnapKit'
  s.dependency 'PromiseKit', "~> 6.8"
  s.dependency 'Alamofire', '~> 5.2'
  s.dependency 'SwiftyJSON', '~> 4.0'
  s.dependency 'Generics'
  s.dependency 'CustomDropDown'
end
